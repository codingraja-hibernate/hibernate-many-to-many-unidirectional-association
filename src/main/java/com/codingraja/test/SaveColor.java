package com.codingraja.test;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.codingraja.domain.Color;

public class SaveColor {

	public static void main(String[] args) {
		
		Configuration configuration = new Configuration();
		configuration.configure("hibernate.cfg.xml");
		
		SessionFactory factory = configuration.buildSessionFactory();
		
		Color color = new Color("Gray");
		Session session = factory.openSession();
		Transaction transaction = session.beginTransaction();
		session.save(color);
		transaction.commit();
		session.close();
		
		System.out.println("Color has been Saved Successfully!");
	}

}
