package com.codingraja.test;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.codingraja.domain.Product;

public class GetProduct {

	public static void main(String[] args) {
		
		Configuration configuration = new Configuration();
		configuration.configure("hibernate.cfg.xml");
		
		SessionFactory factory = configuration.buildSessionFactory();
		
		Session session = factory.openSession();
		Product product = session.get(Product.class, new Long(1));
		
		System.out.println("Product ID: "+product.getId());
		System.out.println("Product Name: "+product.getName());
		System.out.println("Price: "+product.getPrice());
		
		System.out.println("Product Colors: "+product.getColors());
		
		session.close();	
	}

}
