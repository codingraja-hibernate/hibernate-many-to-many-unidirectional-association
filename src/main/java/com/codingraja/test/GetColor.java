package com.codingraja.test;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.codingraja.domain.Color;

public class GetColor {

	public static void main(String[] args) {
		
		Configuration configuration = new Configuration();
		configuration.configure("hibernate.cfg.xml");
		
		SessionFactory factory = configuration.buildSessionFactory();
		
		Session session = factory.openSession();
		Color color = session.get(Color.class, new Long(1));
		
		System.out.println("Color ID: "+color.getId());
		System.out.println("Name: "+color.getName());
		
		session.close();	
	}

}
