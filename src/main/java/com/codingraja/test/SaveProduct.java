package com.codingraja.test;

import java.util.HashSet;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.codingraja.domain.Color;
import com.codingraja.domain.Product;

public class SaveProduct {

	public static void main(String[] args) {
		
		Configuration configuration = new Configuration();
		configuration.configure("hibernate.cfg.xml");
		
		SessionFactory factory = configuration.buildSessionFactory();
		
		Set<Color> colors = new HashSet<Color>();		
		
		Session session = factory.openSession();
		
		colors.add(session.get(Color.class, new Long(4)));
		colors.add(session.get(Color.class, new Long(5)));
		
		Product product = new Product("Iphone", "6", "Apple", 55000.00, colors);
		Transaction transaction = session.beginTransaction();
		session.saveOrUpdate(product);
		transaction.commit();
		session.close();	
		
		System.out.println("Product has been Saved Successfully!");
	}

}
